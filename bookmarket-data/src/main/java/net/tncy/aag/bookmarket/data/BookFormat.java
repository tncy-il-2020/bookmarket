package net.tncy.aag.bookmarket.data;

public enum BookFormat {
    BROCHE,
    POCHE
};
